/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import Home from './src/components/Home';
import {View} from 'react-native';



export default class App extends Component<{}> {
  render() {
    return (
        <Home/>
    );
  }
}
