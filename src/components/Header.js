import React from 'react';
import {
  Text, View
} from 'react-native';

const Header = (props) => {
  const { textStyle, viewstyle } = styles;

  return (
   <View style={viewstyle}>
    <Text style={textStyle}>{props.headerText}</Text>
   </View>
  );
};

const styles = {
  viewstyle: {
    backgroundColor: '#FFFFFF',
    justifyContent: 'space-around',
    alignItems: 'flex-start',
    height: 50,
    paddingTop: 15,
    paddingLeft: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
  },
  textStyle: {
    fontSize: 20,
    fontWeight: '900'
  }
};

export default Header;
