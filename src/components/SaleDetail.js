import React from 'react';
import {
  Text, View, Image, Linking
} from 'react-native';
import Card from './Card';
import CardSection from './CardSection';
import Button from './Button';

const SaleDetail = ({ saledata }) => {
  const { product_name, product_description, product_quoted_price, sale_status, seller_rating,product_image } = saledata;
  const { textcontainer, titleheader, imageStyle, statusheader, priceheader } = styles;

console.log('album detailed');
  return (
  <Card>

  <CardSection>
   <View style={textcontainer}>
   <Text style={titleheader}>{product_name}</Text>
   <Text>{product_description}</Text>
   </View>
  </CardSection>

  <CardSection>
  <Image
  style={imageStyle}
  source={{ uri: 'http://katchup.iqube.io'+product_image }}
  />
  </CardSection>

  <CardSection>
  <Text style={statusheader}>{sale_status}</Text>
  </CardSection>

  <CardSection>
  <Text style={priceheader}>Price: {product_quoted_price.toFixed(2)}</Text>
  </CardSection>

  <CardSection>
  <Button>
  Claim
  </Button>
  </CardSection>


  </Card>
  //whenever we use more than two props references
  //its good to use destructuring components
  //img will not expand so we have to specify width and height
);
};

const styles = {
  textcontainer: {
    justifyContent: 'space-around',
    flexDirection: 'column'
  },
  titleheader: {
    fontSize: 18,
    fontWeight: '500',
    color: '#000000'
  },
  priceheader: {
    fontSize: 17,
    fontWeight: '200',
    color: '#008000',
  },
  statusheader: {
    fontSize: 15,
    alignSelf: 'center',
  },
  thumbnailstyle: {
    height: 50,
    width: 50,
    borderRadius: 3,
    marginRight: 5
  },
  imageStyle: {
    height: 300,
    flex: 1,
    width: null,
    borderRadius: 3,
  }
};

export default SaleDetail;
