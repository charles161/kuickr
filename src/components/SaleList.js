import React, { Component } from 'react';
import {
 ScrollView
} from 'react-native';
import axios from 'axios';
import SaleDetail from './SaleDetail';
import nodata from './nodata';

class SaleList extends Component {

state={ saledata: []};

componentWillMount() {
  axios.get('https://katchup.iqube.io/api/v1/kuickr/get_sales_feed')
  .then(response =>
    this.setState({ saledata: response.data }))
    .catch(()=>{
        this.setState({ saleData: [] });
      });
   console.log('component mounted');
}

renderalbums() {
  if(this.state.saledata!==[]){
  return this.state.saledata.map(saledata =>
    <SaleDetail
    key={saledata.id} saledata={saledata}
    />);
  }
  else {
    return <nodata />
  }
}

  render() {
     console.log(this.state);
  return (
   <ScrollView style={{paddingBottom:10}}>
    {this.renderalbums()}
   </ScrollView>
 );
}
}

export default SaleList;
