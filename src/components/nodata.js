import React from 'react';
import { Text } from 'react-native';

const nodata = () => {
const { textStyle } = styles;

  return (
    <View style={buttonStyle}>
      <Text style={textStyle}>
      Nothing yet!
      </Text>
    </View>
  );
};

const styles = {
  textStyle: {
    alignSelf: 'center',
    fontSize: 19,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10
  },
  containerStyle: {
  flex: 1,
  alignSelf: 'stretch',
  backgroundColor: '#fff',
  marginRight: 5,
  marginLeft: 5
}
};

export default nodata;
