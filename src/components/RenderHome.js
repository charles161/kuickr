import React, {Component} from 'react';
import {
 View,Text
} from 'react-native';
import Header from './Header';
import Intro from './Intro';
import Feed from './Feed';


class RenderHome extends Component {

  render() {
    if (this.props.gotHome==='no') {
      return <Feed/>;
    }
    return (
      <Intro/>
    );
  }
}

export default RenderHome;
