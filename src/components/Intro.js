import React, { Component } from 'react';
import { View, ScrollView, Text} from 'react-native';
import axios from 'axios';
import CardSection from './CardSection';
import Card from './Card';
import SaleList from './SaleList';
import Button from './Button';
import RenderHome from './RenderHome';
import Spinner from './Spinner';

class Intro extends Component {

state = {agreement:'',loading:'true',error: ''};

componentWillMount() {
  axios.get('https://katchup.iqube.io/api/v1/kuickr/get_rules')
  .then(response =>
    this.setState({ agreement: response.data,loading:'false' }));
   console.log('component mounted');
}


onPress() {
  console.log('loginsuccess');
  fetch('https://katchup.iqube.io/api/v1/kuickr/create_kuickr_user?roll_no=15BCS087')
    .then((response) => response.json())
    .then((responseData) => {
      this.setState({
        loading: responseData.status,
      });
    })
    .done();
  }


renderButton() {
  if(this.state.loading==='true'){
    return <Spinner size='small' />
  }
  else if(this.state.loading==='User Kuickr Account Created'){
    return <RenderHome gotHome='yes'/>
  }
  return (
    <Card>

      <CardSection>
        <Text style={{fontWeight: '500',color:'#000',alignItems:'center'}}>
           Terms and Conditions
         </Text>
       </CardSection>

       <CardSection>
         <ScrollView>
           <Text>
            {this.state.agreement}
            </Text>
          </ScrollView>
        </CardSection>

        <Text style={{color: '#FF0000',alignSelf:'center'}}>
         {this.state.error}
         </Text>

        <CardSection>
        <Button onPress={this.onPress.bind(this)}>
          Agree
        </Button>
         </CardSection>

     </Card>
  );
}

  render(){
    return(
      <View>
        {this.renderButton()}
        </View>
    );
  }
}

export default Intro;
