import React, {Component} from 'react';
import {
 View,Text
} from 'react-native';
import Header from './Header';
import RenderHome from './RenderHome';

class Home extends Component {

  render() {
    return (
      <View>
      <Header headerText='Kuickr' />
      <RenderHome gotHome='no'/>
      </View>
    );
  }
}

export default Home;
