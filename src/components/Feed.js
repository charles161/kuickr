import React, { Component } from 'react';
 import {
  ScrollView, Text
 } from 'react-native';
 import CardSection from './CardSection';
 import Card from './Card';
 import Button from './Button';
 import History from './History';
 import PendingSales from './PendingSales';
 import SaleList from './SaleList';

 class Feed extends Component {
   state={tab:'sales'}

   renderContent() {
     switch(this.state.tab){
       case 'history':
          return <History />;
       case 'pending':
          return <PendingSales />;
       case 'sales':
          return <SaleList />;
        default:
          return (<Text>Nothing To Display</Text>);
     }
   }

   onButtonPress() {

  }

    render() {
    return (
      <ScrollView>
          <CardSection>
              <Button>
                Sales Feed
              </Button>
              <Button>
                Pending Sales
              </Button>
              <Button>
                History
              </Button>
           </CardSection>
           {this.renderContent()}
      </ScrollView>
   );
  }

  }


  export default Feed;
